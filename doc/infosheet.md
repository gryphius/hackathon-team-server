Front page
----------

URL: http://hack.local

Gitlab
------

URL: http://gitlab.hack.local

Admin: root / 5iveL!fe


MySQL
-----

Host: mysql.hack.local

Custom http page
----------------

http://web.hack.local


Redis
-----
Host: redis.hack.local


DNS Administration
------------------

http://admin.hack.local/dns


Tech setup
==========


Networking
----------

 * WIFI: hackathon
 * password: codemonkey


 * Gateway: 192.168.250.1
 * DNS:    192.168.250.1
 * DHCP Client Range: 192.168.250.100 - 192.168.250.254


Files and Directories
---------------------

web root: /srv/www
local network share: /srv/projectfiles
install software: /srv/installfiles
public network share(visible to the outside network): /srv/publicfiles


