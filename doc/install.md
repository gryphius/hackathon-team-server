Install basic ubuntu system (this manual is based on ubuntu 15.10 server)

```
sudo su
apt-get install git
cd
git clone https://gitlab.com/gryphius/hackathon-team-server.git
cd hackathon-team-server
```


./install-hackathon-teamserver.sh




setup gitlab

setup gitlab ci runner

vi /etc/hosts

add 127.0.0.1 gitlab.hack.local


root@hackathon-desktop:/tmp# gitlab-ci-multi-runner register
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/ci):
http://gitlab.hack.local/ci
Please enter the gitlab-ci token for this runner:
<token from gitlab page>
Please enter the gitlab-ci description for this runner:
[hackathon-desktop]: local shell
Please enter the gitlab-ci tags for this runner (comma separated):
local,shell
INFO[0018] e381e513 Registering runner... succeeded
Please enter the executor: docker-ssh, ssh, shell, parallels, docker:
shell
INFO[0030] Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
