Stuff to do at home while you still have a fast inet connection
===============================================================

 * clean files/ gitlab projects / dns zones from previous projects
 * download latest software you're planning to use and put it in the installfiles folder
 * pull docker images you think might be useful
