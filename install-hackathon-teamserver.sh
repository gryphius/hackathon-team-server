#!/bin/bash

if [ "$EUID" -ne 0 ]
	then echo "Please run as root"
	exit
fi

cat includes/banner
source includes/functions.sh


installtask basedeps
ansible-playbook -i 127.0.0.1, --connection=local includes/ansible/site.yml

