<?php
//we overwrite dbconfig-common stuff

unset($cfg['Server'][1]);
$cfg['Servers'][1]['host'] = '127.0.0.1';
$cfg['Servers'][1]['auth_type'] = 'config';
$cfg['Servers'][1]['user'] = 'phpmyadmin';
$cfg['Servers'][1]['password'] = 'pma-secret';
$cfg['Servers'][1]['socket'] = '/var/run/mysqld/mysqld.sock';
$cfg['Servers'][1]['connect_type'] = 'socket';