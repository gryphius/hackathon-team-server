textb() { echo $(tput bold)${1}$(tput sgr0); }
greenb() { echo $(tput bold)$(tput setaf 2)${1}$(tput sgr0); }
redb() { echo $(tput bold)$(tput setaf 1)${1}$(tput sgr0); }
yellowb() { echo $(tput bold)$(tput setaf 3)${1}$(tput sgr0); }
pinkb() { echo $(tput bold)$(tput setaf 5)${1}$(tput sgr0); }


installtask() {
	case $1 in
		basedeps)
			echo "$(textb [INFO]) - Installing base dependencies/ansible..."
			apt-get -q update
		    DEBIAN_FRONTEND=noninteractive apt-get -y install software-properties-common
		    apt-add-repository multiverse
            apt-get -q update
            DEBIAN_FRONTEND=noninteractive apt-get -y install ansible python-apt software-properties-common
			if [ "$?" -ne "0" ]; then
				echo "$(redb [ERR]) - Package installation failed"
				exit 1
			fi

		;;


	esac
}
