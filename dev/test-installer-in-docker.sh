#!/bin/bash
cd ../
tar -cvzf dev/dockerconfig/hackathoninstaller.tar.gz install-hackathon-teamserver.sh includes/
cd -
docker build -t hackathondev dockerconfig
docker run -ti hackathondev
